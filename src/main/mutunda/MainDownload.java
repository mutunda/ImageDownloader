/**
 * Created by fotty on 8/1/16.
 */

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class MainDownload {

    public static void main(String[] args) {
        System.out.printf("main");

        try {
            System.out.println("try");
            downloadFile("http://lorempixel.com/400/200/");
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }


    public static File downloadFile(String link) throws IOException {

        if (link.isEmpty()) {
            System.out.printf("link is empty");
            return null;
        } else {
            URL url = new URL(link);
            BufferedImage img = ImageIO.read(url);
            File file = new File("downloaded.jpg");
            ImageIO.write(img, "jpg", file);
            System.out.println("Done");
            return file;
        }
    }
}
