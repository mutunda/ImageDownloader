/**
 * Created by fotty on 8/1/16.
 */

import org.junit.Test;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import java.io.File;
import static org.junit.Assert.*;

public class MainDownloadTest {

    @Test
    public void shouldBeNull() throws Exception{
        MainDownload mainDownload = new MainDownload();
        assertNull("Should be null",mainDownload.downloadFile(""));
    }

    @Test
    public void shoulBeOfTypeImage() throws Exception{
        MainDownload mainDownload = new MainDownload();
        assertThat(mainDownload.downloadFile("http://lorempixel.com/400/200/"), instanceOf(File.class));
    }
}
